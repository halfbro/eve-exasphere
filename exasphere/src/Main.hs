{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE RankNTypes          #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeFamilies        #-}

module Main where

import           Control.Concurrent
import           Control.Monad
import           Reactive.Banana
import           Reactive.Banana.Frameworks
import           System.IO

import           ESI.Auth
import           ESI.Location
import           Exasphere.Extensions
import           Exasphere.Mapper
import           Exasphere.Tracking
import           Exasphere.Types

type Outputs = (LocationUpdateE, SystemsB)

net :: ESIAuthContext -> TickE -> TickE -> Event (Action, SolarSystemId) -> CharUpdateE -> MomentIO Outputs
net ctx eFiveTick eSixtyTick eActionSystem eCharUpdate = do
  TrackingSubsystem bOnlineChars bOn eLocationDeltas <- playerTracker ctx eFiveTick eSixtyTick eCharUpdate
  MappingSubsystem bSystems bConnections <- mapper never eActionSystem never eLocationDeltas
  return (eLocationDeltas, bSystems)

setupNetwork :: AddHandler ()
             -> AddHandler ()
             -> AddHandler ()
             -> AddHandler (Action, SolarSystemId)
             -> AddHandler (CharId, Action)
             ->IO EventNetwork
setupNetwork fiveTick sixtyTick showMap actionSystem charUpdate = do
  ctx <- mkAuthContext
  compile $ do
    eFiveTick   <- fromAddHandler fiveTick
    eSixtyTick  <- fromAddHandler sixtyTick
    eCharUpdate <- fromAddHandler charUpdate
    eShow       <- fromAddHandler showMap
    eSystem     <- fromAddHandler actionSystem
    (eDeltas, bSystems) <- net ctx eFiveTick eSixtyTick eSystem eCharUpdate
    reactimate $ fmap (\_ -> print "Checking for location changes") eFiveTick
    reactimate $ fmap (\_ -> print "Checking for new online players") eSixtyTick
    reactimate $ fmap (\a -> do putStr "Player moved: " ; print a) eDeltas
    reactimate $ fmap print (bSystems <@ eShow)

tickESI ::  Int -> Handler () -> IO ThreadId
tickESI sec f = forkIO $ forever $ do
  f ()
  (threadDelay $ sec * 1100000) -- Use 1100000 to give ESI some buffer

runNetwork :: IO (Handler (), Handler (Action, SolarSystemId))
runNetwork = do
  (tick5, fireTick5) <- newAddHandler
  (tick60, fireTick60) <- newAddHandler
  (charUpdate, fireCharUpdate) <- newAddHandler
  (showMap, fireShowMap) <- newAddHandler
  (actionSystem, fireActionSystem) <- newAddHandler
  net <- setupNetwork tick5 tick60 showMap actionSystem charUpdate
  actuate net
  fireCharUpdate (2113434577, Add)
  fireActionSystem (Add, 30000142)
  tickESI 5  fireTick5
  tickESI 60 fireTick60
  return (fireShowMap, fireActionSystem)

prompt :: (Handler (), Handler (Action, SolarSystemId)) -> IO ()
prompt (fireShowMap, fireActionSystem) = loop
  where loop = do
          putStr "Exa > "
          hFlush stdout
          s <- getLine
          case s of
            "show" -> do putStrLn "Showing Map:" ; fireShowMap ()
            'a':'d':'d':' ':xs -> do
              let id = read xs
              putStrLn $ "Adding system " ++ show id
              fireActionSystem (Add, id)
            's':'u':'b':' ':xs -> do
              let id = read xs
              putStrLn $ "Removing system " ++ show id
              fireActionSystem (Delete, id)
            "quit" -> return ()
            _      -> putStrLn $ s ++ " - unknown command"
          when (s /= "quit") loop

main :: IO ()
main = do
  handlers <- runNetwork
  prompt handlers
  return ()
