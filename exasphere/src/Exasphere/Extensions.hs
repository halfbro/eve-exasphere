module Exasphere.Extensions where

import qualified Data.Map                   as M

import           Control.Concurrent
import           Reactive.Banana
import           Reactive.Banana.Frameworks

mapEventAsyncIO :: (a -> IO b) -> Event a -> MomentIO (Event b)
mapEventAsyncIO f e1 = do
  (e2, handler) <- newEvent
  reactimate $ (\a -> () <$ (forkIO $ f a >>= handler)) <$> e1
  return e2

accumList :: MonadMoment m => (s -> a -> s) -> s -> Event [a] -> m (Behavior s)
accumList f init = accumB init . fmap (flip $ foldl f)
