module Exasphere.Types where

import qualified Data.Map.Strict as M
import qualified Data.Set        as S
import           ESI.Location    (EVELocation (..), ShipInfo (..))
import           Reactive.Banana

type CharId = Integer
type ShipId = Integer
type SolarSystemId = Integer
type CharLocation = Integer

data Action = Add | Delete

-- Fundamentaal tick event, used for making timed calls to the API
type TickE = Event ()

-- Data representing the location and ship type a player is in
data CharacterInfo = CharacterInfo {
  charId   :: CharId,
  ship     :: ShipInfo,
  location :: EVELocation
}

-- Tracking Subsystem related types
type CharInfoB = Behavior (M.Map CharId CharacterInfo)
type OnlineCharIdsB = Behavior (S.Set CharId)
type LocationUpdateE = Event [(SolarSystemId, SolarSystemId)]

data TrackingSubsystem = TrackingSubsystem CharInfoB
                                           OnlineCharIdsB
                                           LocationUpdateE

-- Mapping Subsystem related types
type Connection = (SolarSystemId, SolarSystemId)

type SystemsB = Behavior (S.Set SolarSystemId)
type ConnectionsB = Behavior [Connection]
data MappingSubsystem = MappingSubsystem SystemsB ConnectionsB

nullLocation :: EVELocation
nullLocation = EVELocation 0 Nothing Nothing

nullShip :: ShipInfo
nullShip = ShipInfo 0 0 "ERROR"
