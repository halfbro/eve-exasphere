{-# LANGUAGE RecursiveDo #-}
module Exasphere.Mapper where

import           Control.Monad.Fix
import           Data.Either
import           Data.Foldable
import           Data.Maybe
import qualified Data.Set                    as S
import           Reactive.Banana
import           Reactive.Banana.Combinators
import           Reactive.Banana.Frameworks

import           Exasphere.Extensions
import           Exasphere.Types

data Lifetime = Stable | EOL
data Mass = Fresh | Massed | Crit

splitAction :: Ord a => Event (Action, a) -> Event (S.Set a -> S.Set a)
splitAction = fmap act
  where act (Add, a)    = S.insert a
        act (Delete, a) = S.delete a

bPinnedSystems :: MonadMoment m => Event (Action, SolarSystemId) -> m (Behavior (S.Set SolarSystemId))
bPinnedSystems eActions = accumB S.empty $ splitAction eActions

isNewSystem :: S.Set SolarSystemId -> (SolarSystemId, SolarSystemId) -> Maybe SolarSystemId
isNewSystem s conn = if S.member (fst conn) s
                     then Just (snd conn)
                     else Nothing

checkForNewSystems :: Behavior (S.Set SolarSystemId) -> LocationUpdateE -> Event [SolarSystemId]
checkForNewSystems = apply . fmap mapMaybe . fmap isNewSystem

bMappedSystems :: (MonadFix m, MonadMoment m)
               => Event (Action, SolarSystemId)
               -> Event (Action, SolarSystemId)
               -> LocationUpdateE
               -> m (Behavior (S.Set SolarSystemId))
bMappedSystems eManualAction eFromConnAction eLocationUpdate = mdo
  let eNewUpdates = checkForNewSystems out eLocationUpdate
  out <- accumB S.empty $ unions [ splitAction eManualAction,
                                   splitAction eFromConnAction,
                                   addSystems eNewUpdates
                                 ]
  return out
  where
    addSystems = fmap $ S.union . S.fromList

combineSystems :: Behavior (S.Set SolarSystemId) -> Behavior (S.Set SolarSystemId) -> Behavior (S.Set SolarSystemId)
combineSystems bPinned bMapped = fmap S.union bPinned <*> bMapped

mapper :: (MonadFix m, MonadMoment m)
       => Event (Action, SolarSystemId)
       -> Event (Action, SolarSystemId)
       -> Event (Action, (SolarSystemId, SolarSystemId))
       -> LocationUpdateE
       -> m MappingSubsystem
mapper ePinAction eMappedAction eConnAction eLocationUpdate = do
  bMap <- bMappedSystems eMappedAction never eLocationUpdate
  bPin <- bPinnedSystems ePinAction
  let bSystems = combineSystems bMap bPin
  return $ MappingSubsystem bSystems (pure [])






