module Exasphere.Tracking where

import           Control.Concurrent
import           Control.Concurrent.Async
import           Data.Either
import           Data.Foldable
import qualified Data.Map.Strict             as M
import           Data.Maybe
import qualified Data.Set                    as S
import           Reactive.Banana
import           Reactive.Banana.Combinators
import           Reactive.Banana.Frameworks

import           ESI.Auth                    (ESIAuthContext)
import           ESI.Location                (EVELocation (..), ShipInfo (..),
                                              getLocation, getOnlineStatus,
                                              getShip, online)

import           Exasphere.Extensions
import           Exasphere.Types


type CharUpdateE = Event (CharId, Action)
type TrackedCharsB = Behavior (S.Set CharId)

-- Makes a behavior representing players being tracked
-- This just needs an event which represents a character addition/removal
makeTracker :: MonadMoment m
              => CharUpdateE
              -> m TrackedCharsB
makeTracker = accumB S.empty . fmap update
  where update (id, Add)  = S.insert id
        update (id, Delete) = S.delete id

-- Makes a behavior representing the current online players
-- Needs a sixty second tick and a list of character ids to track
makeOnlineChecker :: ESIAuthContext
               -> TickE
               -> TrackedCharsB
               -> MomentIO OnlineCharIdsB
makeOnlineChecker ctx eSixtyTick bTrackedChars = do
  let eCheckOnline = S.toList <$> bTrackedChars <@ eSixtyTick
  eOnlineUpdate <- mapEventAsyncIO filterChars eCheckOnline
  stepper S.empty eOnlineUpdate
  where
    filterChars ps = S.fromList <$> map fst <$> filter snd <$> checkAllChars ps
    checkAllChars = mapConcurrently checkSingleChar
    checkSingleChar p = do
      isOnline <- either (\_ -> False) online <$> getOnlineStatus ctx p
      return (p, isOnline)

eOnlineCharInfo :: ESIAuthContext
                -> TickE
                -> OnlineCharIdsB
                -> MomentIO (Event [CharacterInfo])
eOnlineCharInfo ctx eFiveTick bOnlineCharIds = do
  let eOnlinePlayers = S.toList <$> bOnlineCharIds <@ eFiveTick
  mapEventAsyncIO getAllChars eOnlinePlayers
  where
    getSingleCharEvent e = mapEventAsyncIO getSingleChar e
    getAllChars = mapConcurrently getSingleChar
    getSingleChar id = do
      location <- fromRight nullLocation <$> getLocation ctx id
      ship <- fromRight nullShip <$> getShip ctx id
      return $ CharacterInfo id ship location

bCharData :: MonadMoment m => Event [CharacterInfo] -> m CharInfoB
bCharData = accumList updateCharInfo M.empty
  where updateCharInfo m c = M.insert (charId c) c m

bLocations :: MonadMoment m
           => Event [CharacterInfo]
           -> m (Behavior (M.Map CharId SolarSystemId))
bLocations = accumList updateLocation M.empty
  where updateLocation m c = M.insert (charId c) (solar_system_id $ location c) m

isCharLocationDifferent :: M.Map CharId SolarSystemId
                        -> CharacterInfo
                        -> Maybe (SolarSystemId, SolarSystemId)
isCharLocationDifferent m cinfo = M.lookup (charId cinfo) m >>= diff
  where diff oldLocation = if newLocation /= oldLocation
                        then Just (oldLocation, newLocation)
                        else Nothing
        newLocation = systemId cinfo
        systemId = solar_system_id . location

-- Using the current list of player locations, output all
-- position changes as (origin, destination)
findLocationChanges :: Behavior (M.Map CharId SolarSystemId)
                    -> Event [CharacterInfo]
                    -> LocationUpdateE
findLocationChanges = apply . fmap mapMaybe . fmap isCharLocationDifferent

playerTracker :: ESIAuthContext ->
                 TickE ->
                 TickE ->
                 CharUpdateE ->
                 MomentIO TrackingSubsystem
playerTracker ctx eFiveTick eSixtyTick eCharUpdate = do
  bTracked <- makeTracker eCharUpdate
  bOnline <- makeOnlineChecker ctx eSixtyTick bTracked
  let bCharIdList = fmap S.toList bOnline
  eCharInfo <- eOnlineCharInfo ctx eFiveTick bOnline
  bCharInfo <- bCharData eCharInfo
  locations <- bLocations eCharInfo
  let eChanges = filterE (not . null) $ findLocationChanges locations eCharInfo
  return $ TrackingSubsystem bCharInfo bOnline eChanges
