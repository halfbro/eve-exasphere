import Test.Hspec
import ESI.Alliance
import Network.HTTP.Client --(newManager)
import Network.HTTP.Client.TLS (tlsManagerSettings)
import Servant.Client hiding (responseBody)

setupEnv :: IO ClientEnv
setupEnv = do
  let managerSettings = tlsManagerSettings
  m <- newManager managerSettings {
    --managerModifyRequest = \r -> do print r; return r,
    managerModifyResponse = \r -> do return r }
  return $ ClientEnv m (BaseUrl Https "esi.tech.ccp.is" 443 "/latest") Nothing

main :: IO ()
main = hspec spec

spec :: Spec
spec =
  beforeAll setupEnv $ do
    describe "/alliances/" $
      it "Returns the list of all alliance ids on GET" $ \env ->
        getAllianceIDs env `shouldNotReturn` []

    describe "/alliances/names/" $ do
      it "Returns a tuple corresponding to (ID, name) on GET with single parameter" $ \env ->
        getAllianceNamesFromIDs env [99006690]
          `shouldReturn`
          [(99006690, "Mouth Trumpet Cavalry")]
      it "Returns a list of tuples corresponding to (ID, name) on GET with multiple parameters" $ \env ->
        getAllianceNamesFromIDs env [99006690, 2, 3, 4]
          `shouldReturn` [
            (99006690, "Mouth Trumpet Cavalry")
            ,(2, "EVE Central Bank")
            ,(3, "EVE Market")
            ,(4, "EVE Station")
          ]

    describe "/alliances/{alliance_id}/" $
      it "Returns public information about an alliance on GET" $ \env -> do
        getAllianceFromID env 99006690
          `shouldReturn`
          (Just $ Alliance "Mouth Trumpet Cavalry" 958401507 98279717 "MCAV" "2016-07-21T14:21:55Z" 98455787)
        getAllianceFromID env 99006691
          `shouldReturn`
          (Just $ Alliance "Voynich Intelligence Agency" 96388896 98462506 "VIA" "2016-07-22T01:33:15Z" 98462506)

    describe "/alliances/{alliance_id}/corporations/" $
      it "Returns corporations under an alliance on GET" $ \env -> do
        getAllianceCorporationsFromID env 99006690
          `shouldReturn`
          [ 98275753, 98279717, 98341909, 98455787, 98504356]
        getAllianceCorporationsFromID env 99006691
          `shouldReturn`
          []

    describe "/alliances/{alliance_id}/icons/" $
      it "Returns endpoints to an alliance's icons on GET" $ \env -> do
        getAllianceIconsFromID env 99006690
          `shouldReturn`
          Just (IconEndpoints
                 "http://image.eveonline.com/Alliance/99006690_128.png"
                 "http://image.eveonline.com/Alliance/99006690_64.png" )
        getAllianceIconsFromID env 99006691
          `shouldReturn`
          Just (IconEndpoints
                 "http://image.eveonline.com/Alliance/99006691_128.png"
                 "http://image.eveonline.com/Alliance/99006691_64.png" )
