import Test.Hspec
import ESI.Routes
import Network.HTTP.Client (Request, newManager, managerModifyRequest)
import Network.HTTP.Client.TLS (tlsManagerSettings)
import Servant.Client

printRequest :: Request -> IO Request
printRequest req = do
  print req
  return req

setupEnv :: IO ClientEnv
setupEnv = do
  let managerSettings = tlsManagerSettings --{managerModifyRequest = printRequest}
  manager <- newManager managerSettings
  return $ ClientEnv manager (BaseUrl Https "esi.tech.ccp.is" 443 "/latest") Nothing

main :: IO ()
main = hspec spec

spec :: Spec
spec =
  beforeAll setupEnv $
    describe "/route/{origin}/{destination}/" $ do
      it "Returns the systems on a route between origin and destination systems on GET" $ \env ->
        getRouteUsing env 30002771 30002772 Nothing Nothing "shortest" `shouldReturn` [30002771,30002770,30002769,30002772]
      it "Returns systems on a route while avoiding some systems" $ \env ->
        getRouteUsing env 30002771 30002772 (Just [30002770]) Nothing "shortest" `shouldReturn` [30002771, 30002773, 30002774, 30002772]
      it "Returns systems on a route using systems connected to each other" $ \env ->
        getRouteUsing env 30002771 30002772 Nothing (Just [(30002771,30002769)]) "shortest" `shouldReturn` [30002771, 30002769, 30002772]
      it "Returns systems on a route using the most secure route" $ \env ->
        getRouteUsing env 30002771 30002772 Nothing Nothing "secure" `shouldReturn` [30002771, 30002773, 30002774, 30002772]
