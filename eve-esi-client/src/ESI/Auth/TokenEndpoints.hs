{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}

module ESI.Auth.TokenEndpoints
  ( getInitialToken
  , refreshESIToken
  , makeAuthEnvironment
  ) where

import           ESI.Auth.Data

import           Data.Proxy
import           Network.HTTP.Client (newManager, managerModifyRequest)
import           Network.HTTP.Client.TLS (tlsManagerSettings)
import           Servant.API
import           Servant.Client
import qualified Data.ByteString.Char8 as BS

type AuthAPI =
  "oauth" :> "token" :>
    QueryParam "grant_type" String :>
    QueryParam "code" AuthorizationCode :>
    BasicAuth "esi-oauth" Bool :>
    Post '[JSON] ESIToken
  :<|>
  "oauth" :> "token" :>
    QueryParam "grant_type" String :>
    QueryParam "refresh_token" RefreshToken :>
    BasicAuth "esi-oauth" Bool :>
    Post '[JSON] ESIToken

authAPI :: Proxy AuthAPI
authAPI = Proxy

getInitialToken' :: Maybe String
                 -> Maybe AuthorizationCode
                 -> BasicAuthData
                 -> ClientM ESIToken

refreshESIToken' :: Maybe String
                 -> Maybe RefreshToken
                 -> BasicAuthData
                 -> ClientM ESIToken

getInitialToken' :<|> refreshESIToken' = client authAPI

getInitialToken :: ClientEnv
                -> AuthorizationCode
                -> ESISecrets
                -> IO (Either ServantError ESIToken)
getInitialToken env d (ESISecrets cId key) =
  runClientM
  (getInitialToken' (Just "authorization_code") (Just d) auth)
  env
  where auth = BasicAuthData (BS.pack cId) (BS.pack key)

refreshESIToken :: ClientEnv
                -> RefreshToken
                -> ESISecrets
                -> IO (Either ServantError ESIToken)
refreshESIToken env t (ESISecrets cId key) =
  runClientM
    (refreshESIToken' (Just "refresh_token") (Just t) auth)
    env
  where auth = BasicAuthData (BS.pack cId) (BS.pack key)

makeAuthEnvironment :: IO ClientEnv
makeAuthEnvironment = do
  let base = BaseUrl Https "login.eveonline.com" 443 ""
  let m = tlsManagerSettings
  manager <- newManager m
  return $ ClientEnv manager base Nothing
