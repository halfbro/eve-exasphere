{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

module ESI.Auth.Data where

import           Data.Aeson
import           Data.Time
import           GHC.Generics

type AccessToken = String
type RefreshToken = String

type ClientID = String
type SecretKey = String

type ErrString = String
type CharacterID = Integer

type AuthorizationCode = String

data ESISecrets = ESISecrets {
  clientID :: ClientID,
  secretKey :: SecretKey
} deriving (Eq, Show, Generic)
instance FromJSON ESISecrets

data ESIToken = ESIToken {
  accessToken :: AccessToken,
  tokenType :: String,
  expiresIn :: DiffTime,
  refreshToken :: RefreshToken
} deriving (Show, Generic)
instance FromJSON ESIToken where
  parseJSON = withObject "ESIToken" $ \v -> ESIToken
    <$> v .: "access_token"
    <*> v .: "token_type"
    <*> v .: "expires_in"
    <*> v .: "refresh_token"
