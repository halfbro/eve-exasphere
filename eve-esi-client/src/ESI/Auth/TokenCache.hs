{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE DeriveGeneric  #-}
{-# LANGUAGE DeriveFunctor  #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module ESI.Auth.TokenCache
  ( new
  , lookupToken
  , runESIAuthM
  , TokenCache (..)
  , ESIAuthContext (..)
  , ESIAuthM (..)
  ) where

import           ESI.Auth.Data
import           ESI.Auth.TokenEndpoints

import           Servant.Client
import           Servant.Client.Core.Internal.Auth
import           Servant.Client.Core.Internal.Request
import           GHC.Generics
import           Control.Applicative
import           Control.Concurrent
import           Control.Concurrent.STM
import           Control.Monad.IO.Class
import           Control.Monad.Trans.Maybe
import           Control.Monad.Trans.Except
import           Control.Monad.Base
import           Control.Monad.Reader.Class
import           Control.Monad.Trans.Reader hiding (ask)
import           Control.Monad.Trans.Class
import qualified STMContainers.Map as STM

data ESIAuthContext = ESIAuthContext
  { cache :: TokenCache
  , authSecrets :: ESISecrets
  , apiEnv :: ClientEnv
  , fetch :: CharacterID -> IO (Maybe RefreshToken)
  }

newtype ESIAuthM a = ESIAuthM
  { runESIAuthM' :: ReaderT ESIAuthContext ClientM a }
  deriving ( Functor, Applicative, Monad, MonadIO, Generic,
             MonadReader ESIAuthContext)

runESIAuthM :: ESIAuthM a -> ESIAuthContext -> IO (Either ServantError a)
runESIAuthM m ctx = runClientM cm env
  where
  env = apiEnv ctx
  cm = runReaderT (runESIAuthM' m) ctx

instance MonadBase ClientM ESIAuthM where
  liftBase = ESIAuthM . lift

data CharacterInfo = CharacterInfo
    { charID :: CharacterID
    , refreshToken :: RefreshToken }
    deriving Show

newtype TokenCache = TokenCache {toMap :: STM.Map CharacterID AccessToken}

new :: IO TokenCache
new = TokenCache <$> atomically STM.new

insert :: CharacterID -> AccessToken -> TokenCache -> IO ()
insert cid token = atomically . STM.insert token cid . toMap

delete :: CharacterID -> TokenCache -> IO ()
delete cid = atomically . STM.delete cid . toMap

lookupToken :: CharacterID
            -> ESIAuthM (Maybe AccessToken)
lookupToken char = do
  (ESIAuthContext c s _ f) <- ask
  liftIO $ runMaybeT $ getCachedToken c <|> getNewToken f c s
  where
    getCachedToken cache = MaybeT . liftIO . atomically $ STM.lookup char (toMap cache)
    getNewToken fetch cache secrets = do
      refToken <- MaybeT $ fetch char
      env <- liftIO makeAuthEnvironment
      let ref = refreshESIToken env refToken secrets
      (ESIToken newToken _ _ _) <- exceptToMaybeT . ExceptT $ ref
      liftIO $ insert char newToken cache
      liftIO $ forkIO $ expireToken cache
      return newToken
    expireToken cache = do threadDelay (1200*1000*1000); delete char cache
