{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE DeriveGeneric #-}

module ESI.Location where

import           ESI.Auth

import           Data.Aeson
import           Data.Bifunctor
import           GHC.Generics
import           Servant
import           Servant.API
import           Servant.Client
import           Servant.Client.Core.Internal.Auth

data ESIError = ESIError {
  e :: ServantError
} deriving (Show)

data LoginInfo = LoginInfo {
  online :: Bool,
  last_login :: String,
  last_logout :: String,
  logins :: Integer
} deriving (Show, Generic)
instance FromJSON LoginInfo

data EVELocation = EVELocation {
  solar_system_id :: Integer,
  station_id :: Maybe Integer,
  structure_id :: Maybe Integer
} deriving (Show, Generic, Eq, Ord)
instance FromJSON EVELocation

data ShipInfo = ShipInfo {
  ship_type_id :: Integer,
  ship_item_id :: Integer,
  ship_name :: String
} deriving (Show, Generic)
instance FromJSON ShipInfo

type LocationApi =
  "characters" :>
    Capture "character_id" Integer :>
    QueryParam "datasource" String :>
    AuthProtect "esi-auth" :>
    "online" :>
    Get '[JSON] LoginInfo
  :<|>
  "characters" :>
    Capture "character_id" Integer :>
    QueryParam "datasource" String :>
    AuthProtect "esi-auth" :>
    "location" :>
    Get '[JSON] EVELocation
  :<|>
  "characters" :>
    Capture "character_id" Integer :>
    QueryParam "datasource" String :>
    AuthProtect "esi-auth" :>
    "ship" :>
    Get '[JSON] ShipInfo

locationApi :: Proxy LocationApi
locationApi = Proxy

getOnline'   :: Integer -> Maybe String -> AuthenticatedRequest (AuthProtect "esi-auth") -> ClientM LoginInfo
getLocation' :: Integer -> Maybe String -> AuthenticatedRequest (AuthProtect "esi-auth") -> ClientM EVELocation
getShip'     :: Integer -> Maybe String -> AuthenticatedRequest (AuthProtect "esi-auth") -> ClientM ShipInfo
getOnline'
  :<|> getLocation'
  :<|> getShip' = client locationApi

-- Exported functions
getOnlineStatus :: ESIAuthContext -> Integer -> IO (Either ESIError LoginInfo)
getOnlineStatus ctx cID = do
  first ESIError <$> runESIAuthM
    (runWithChar cID $ getOnline' cID (Just "tranquility"))
    ctx

getLocation :: ESIAuthContext -> Integer -> IO (Either ESIError EVELocation)
getLocation ctx cID =
  first ESIError <$> runESIAuthM
    (runWithChar cID $ getLocation' cID (Just "tranquility"))
    ctx

getShip :: ESIAuthContext -> Integer -> IO (Either ESIError ShipInfo)
getShip ctx cID =
  first ESIError <$> runESIAuthM
    (runWithChar cID $ getShip' cID (Just "tranquility"))
    ctx
