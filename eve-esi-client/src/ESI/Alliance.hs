{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DuplicateRecordFields #-}

module ESI.Alliance where

-- testing imports
import ESI.Auth

import qualified Data.Text as T
import Data.List
import Data.Aeson
import Data.Proxy
import GHC.Generics
import Servant.API
import Servant.Client

-- Types

type SourceServer = String
type AllianceID = Integer
type CorporationID = Integer

instance ToHttpApiData [AllianceID] where
  toUrlPiece = T.pack . intercalate "," . map show
  toQueryParam = toUrlPiece

data AllianceNameRes = AllianceNameRes {
    alliance_id :: AllianceID,
    alliance_name :: String
  }
  deriving (Eq, Show, Generic)
instance FromJSON AllianceNameRes

data Alliance = Alliance {
    name :: String,
    creator_id :: Integer,
    creator_corporation_id :: CorporationID,
    ticker :: String,
    date_founded :: String,
    executor_corporation_id :: CorporationID
  }
  deriving (Eq, Show, Generic)
instance FromJSON Alliance

data IconEndpoints = IconEndpoints {
    px128x128 :: String,
    px64x64   :: String
  }
  deriving (Eq, Show, Generic)
instance FromJSON IconEndpoints

-- API Definition

type AllianceApi =
  "alliances" :>
    QueryParam "datasource" SourceServer :>
    Get '[JSON] [AllianceID]
    :<|>
  "alliances" :> "names":>
    QueryParam "datasource" SourceServer :>
    QueryParam "alliance_ids" [AllianceID] :>
    Get '[JSON] [AllianceNameRes]
    :<|>
  "alliances" :>
    Capture "alliance_id" AllianceID :>
    QueryParam "datasource" SourceServer :>
    Get '[JSON] Alliance
    :<|>
  "alliances" :>
    Capture "alliance_id" AllianceID :>
    "corporations" :>
    QueryParam "datasource" SourceServer :>
    Get '[JSON] [CorporationID]
    :<|>
  "alliances" :>
    Capture "alliance_id" AllianceID :>
    "icons" :>
    QueryParam "datasource" SourceServer :>
    Get '[JSON] IconEndpoints

allianceApi :: Proxy AllianceApi
allianceApi = Proxy

getAlliances :: Maybe SourceServer -> ClientM [AllianceID]
getAllianceNames :: Maybe SourceServer -> Maybe [AllianceID] -> ClientM [AllianceNameRes]
getAlliance :: AllianceID -> Maybe SourceServer -> ClientM Alliance
getCorporations :: AllianceID -> Maybe SourceServer -> ClientM [CorporationID]
getIconEndpoints :: AllianceID -> Maybe SourceServer -> ClientM IconEndpoints
getAlliances :<|>
  getAllianceNames :<|>
  getAlliance :<|>
  getCorporations :<|>
  getIconEndpoints = client allianceApi

-- Exported functions

getAllianceIDs :: ClientEnv -> IO [AllianceID]
getAllianceIDs env = do
  res <- runClientM
           (getAlliances $ Just "tranquility")
           env
  case res of
    Left _ ->
      return []
    Right alliances -> return alliances

getAllianceNamesFromIDs :: ClientEnv -> [AllianceID] -> IO [(AllianceID, String)]
getAllianceNamesFromIDs env ids = do
  res <- runClientM
           (getAllianceNames (Just "tranquility") (Just ids))
           env
  case res of
    Left _ ->
      return []
    Right alliances -> do
      let tuples = map (\x -> (alliance_id x, alliance_name (x :: AllianceNameRes) )) alliances
      return tuples

getAllianceFromID :: ClientEnv -> AllianceID -> IO (Maybe Alliance)
getAllianceFromID env aID = do
  res <- runClientM
           (getAlliance aID (Just "tranquility"))
           env
  case res of
    Left _ -> return Nothing
    Right alliance -> return $ Just alliance

getAllianceCorporationsFromID :: ClientEnv -> AllianceID -> IO [CorporationID]
getAllianceCorporationsFromID env aID = do
  res <- runClientM
           (getCorporations aID (Just "tranquility"))
           env
  case res of
    Left _ -> return []
    Right corps -> return corps

getAllianceIconsFromID :: ClientEnv -> AllianceID -> IO (Maybe IconEndpoints)
getAllianceIconsFromID env aID = do
  res <- runClientM
         (getIconEndpoints aID (Just "tranquility"))
         env
  case res of
    Left _ -> return Nothing
    Right icons -> return $ Just icons
