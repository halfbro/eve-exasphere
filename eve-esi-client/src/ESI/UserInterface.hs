{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}

module ESI.UserInterface where

import ESI.Auth

import           Servant
import           Servant.API
import           Servant.Client
import           Servant.Client.Core.Internal.Auth

type SolarSystemID = Integer

type UserInterfaceApi =
  "ui" :>
    "autopilot" :>
      "waypoint" :>
        QueryParam "datasource" String :>
        QueryParam "add_to_beginning" Bool :>
        QueryParam "clear_other_waypoints" Bool :>
        QueryParam "destination_id" SolarSystemID :>
        AuthProtect "esi-auth" :>
        Post '[JSON] NoContent

userInterfaceApi :: Proxy UserInterfaceApi
userInterfaceApi = Proxy

setWaypoint' :: Maybe String
             -> Maybe Bool
             -> Maybe Bool
             -> Maybe SolarSystemID
             -> AuthenticatedRequest (AuthProtect "esi-auth")
             -> ClientM NoContent
setWaypoint' = client userInterfaceApi

setWaypoint :: ESIAuthContext -> Integer -> Bool -> Bool -> SolarSystemID -> IO (Either ServantError NoContent)
setWaypoint ctx cID addBeginning clear dest =
  runESIAuthM
    (runWithChar cID $ setWaypoint'
                        (Just "tranquility")
                        (Just addBeginning)
                        (Just clear)
                        (Just dest) )
    ctx

