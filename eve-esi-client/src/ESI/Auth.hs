{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}

module ESI.Auth
  ( runWithChar
  , mkAuthContext
  , runESIAuthM
  , ESIAuthContext (..)
  -- temp functions
  , totallyDBCall
  ) where

import           ESI.Auth.Data
import           ESI.Auth.TokenCache
import           ESI.Auth.TokenEndpoints

import           GHC.Generics
import           Servant hiding (addHeader)
import           Servant.Client
import           Servant.Client.Core.Internal.Auth
import           Servant.Client.Core.Internal.Request
import           Network.HTTP.Client (newManager)
import           Network.HTTP.Client.TLS (tlsManagerSettings)
import           Data.Time
import           Data.Text
import           Data.Aeson
import           Data.Aeson.Parser
import           Data.Aeson.Types
import           Control.Monad.Base
import           Control.Monad.Trans.Reader hiding (ask)
import qualified Data.ByteString.Lazy as BS

type instance AuthClientData (AuthProtect "esi-auth") = AccessToken

authenticateESIReq :: AccessToken -> Request -> Request
authenticateESIReq t = addHeader "Authorization" $ "Bearer " ++ t

runWithChar :: CharacterID
            -> (AuthenticatedRequest (AuthProtect "esi-auth") -> ClientM a)
            -> ESIAuthM a
runWithChar cID f = do
  m <- lookupToken cID
  case m of
    Nothing -> liftBase . throwError $ ConnectionError "No refresh token for character found"
    Just updatedToken ->
      liftBase $ f (mkAuthenticatedRequest updatedToken authenticateESIReq)

mkApiEnv :: IO ClientEnv
mkApiEnv = do
  let base = BaseUrl Https "esi.tech.ccp.is" 443 "latest"
  let m = tlsManagerSettings
  manager <- newManager m
  return $ ClientEnv manager base Nothing

mkAuthContext :: IO ESIAuthContext
mkAuthContext = do
  cache <- new
  (Just secrets) <- decode <$> BS.readFile ".env"
  apienv <- mkApiEnv
  return $ ESIAuthContext cache secrets apienv fetch
  where fetch = totallyDBCall

------------------ Test functions

-- The value here ("Zrrt...") is gotten from the getInitialToken function
-- This is a refresh token, but you need the clientID and secretKey to get a new one
totallyDBCall :: CharacterID -> IO (Maybe RefreshToken)
totallyDBCall _ = return $ Just "wQWtzJrA8A8tSEm1-6thXT6l2edxkUjdLQXfzy8uFknkmKn1GBs-s7YpT-Ty8yu70"

