{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE OverloadedStrings #-}

module ESI.Routes where

import qualified Data.Text as T
import Data.List
import Data.Proxy
import Servant.API
import Servant.Client

-- Types

type SourceServer = String
type SolarSystemID = Integer
type Security = String
type SystemConnection = (SolarSystemID,SolarSystemID)

instance ToHttpApiData [SolarSystemID] where
  toUrlPiece = T.pack . intercalate "," . map show

instance ToHttpApiData [SystemConnection] where
  toUrlPiece = let showpair p = show (fst p) ++ "|" ++ show (snd p)
               in T.pack . intercalate "," . map showpair

-- API Definition

type RoutesApi =
  "route" :>
    Capture "origin"      SolarSystemID :>
    Capture "destination" SolarSystemID :>
    QueryParam "datasource"  SourceServer :>
    QueryParam "avoid"       [SolarSystemID] :>
    QueryParam "connections" [SystemConnection] :>
    QueryParam "flag"        Security :>
    Get '[JSON] [SolarSystemID]

routesApi :: Proxy RoutesApi
routesApi = Proxy

getRoute :: SolarSystemID ->
            SolarSystemID ->
            Maybe SourceServer ->
            Maybe [SolarSystemID] ->
            Maybe [SystemConnection] ->
            Maybe Security ->
            ClientM [SolarSystemID]
getRoute = client routesApi

-- Exported functions

getRouteUsing :: ClientEnv ->
                 SolarSystemID ->
                 SolarSystemID ->
                 Maybe [SolarSystemID] ->
                 Maybe [SystemConnection] ->
                 Security ->
                 IO [SolarSystemID]
getRouteUsing env origin destination avoid connections sec = do
  res <- runClientM
           (getRoute origin destination (Just "tranquility") avoid connections (Just sec))
           env
  case res of
    Left _ ->
      return []
    Right systems -> return systems
